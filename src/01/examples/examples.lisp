(defpackage #:paip/01/examples
  (:nicknames #:01/examples #:paip.01.examples #:01.examples)
  (:use #:cl)
  (:import-from #:paip/utils
                #:mappend))

(in-package #:paip/01/examples)

(defparameter *titles* '(mr ms mrs miss sir madam admiral major general)
  "A list of titles that can appear at the start of a name.")

(defun last-name (name)
  "Select the last name from a NAME represented as a list."
  (first (last name)))

(defun first-name (name)
  "Select the first name from a NAME represented as a list."
  (if (member (first name) *titles*)
      (first-name (rest name))
      (first name)))

(defun self-and-double (x) (list x (+ x x)))

(defun numbers-and-negations (input)
  "Given an INPUT list, return each number and its negation."
  (mappend #'number-and-negation input))

(defun number-and-negation (x)
  "If X is a number, return a list of X and -X."
  (if (numberp x)
      (list x (- x))))
