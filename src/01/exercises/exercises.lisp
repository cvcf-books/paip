(defpackage #:paip/01/exercises
  (:nicknames #:01/exercises #:paip.01.exercises #:01.exercises)
  (:use #:cl)
  (:import-from #:paip/utils
                #:last1
                #:flatten))

(in-package #:paip/01/exercises)

;;;;
;; Exercise 01

(defparameter *suffixes* '(md sr jr i ii iii iv v vi vii viii ix x)
  "A list of suffixes that can appear at the end of a name.")

(defun last-name (name)
  "Select the last name from a NAME represented as a list."
  (if (member (last1 name) *suffixes*)
      (last-name (butlast name))
      (last1 name)))

;;;;
;; Exercise 02

(defun power (base exponent)
  "Raise BASE to the (positive) EXPONENT power."
  (labels ((rec (acc exponent)
             (when (minusp exponent)
               (error "Expected EXPONENT to be non-negative but got: ~d" exponent))
             (if (= exponent 0)
                 acc
                 (rec (* base acc) (1- exponent)))))
    (rec 1 exponent)))


;;;;
;; Exercise 03

(defun count-atoms (form)
  "Count all the non-NIL atoms in FORM."
  (labels ((rec (acc form)
             (cond ((null form) acc)
                   ((atom form) (1+ acc))
                   (t (+ (rec acc (first form)) (rec acc (rest form)))))))
    (rec 0 form)))

(defun count-atoms/2 (form)
  "Count all the non-NIL atoms in FORM."
  (length (flatten form)))

;;;;
;; Exercise 04

(defun count-anywhere (exp form &key (test #'eql))
  "Count the number of times an expression EXP appears in FORM."
  (labels ((rec (acc form)
             (cond ((funcall test exp form) (1+ acc))
                   ((atom form) acc)
                   (t (+ (rec acc (first form)) (rec acc (rest form)))))))
    (rec 0 form)))

(defun count-anywhere/2 (exp form &key (test #'eql))
  "Count the number of times an expression EXP appears in FORM."
  (length (remove-if-not (lambda (x) (funcall test x exp))
                         (flatten form))))

;;;;
;; Exercise 05

(defun dot-product (v1 v2)
  "Return the dot product of V1 and V2."
  (reduce #'+ (mapcar #'* v1 v2)))
