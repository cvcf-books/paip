(defpackage #:paip/03/exercises
  (:nicknames #:03/exercises #:paip.03.exercises #:03.exercises)
  (:use #:cl))

(in-package #:paip/03/exercises)

;;;;
;; Exercises 01

((lambda (x)
   ((lambda (y)
      (+ x y))
    (* x x)))
 6)

;;;;
;; Exercise 02

;; CONS is a special case of LIST*

;;;;
;; Exercise 03

(defun princ-dotted-pair (form)
  "Print FORM in dotted-pair notation."
  ;; Could also use `format' to do this but the instructions specifically say to
  ;; use `princ'.
  (with-output-to-string (ss)
    (labels ((rec (form)
               (cond ((consp form)
                      (princ "(" ss)
                      (rec (first form))
                      (princ " . " ss)
                      (rec (rest form))
                      (princ ")" ss))
                     (t (princ form ss)))))
      (rec form))))

;;;;
;; Exercise 04

(defun print-form (form)
  "Print FORM in dotted-pair notation, if necessary; otherwise print FORM
normally."
  (with-output-to-string (ss)
    (labels ((first-prefix (form)
               (if (atom (first form)) "" "("))
             (first-suffix (form)
               (if (atom (first form)) "" ")"))
             (rest-prefix (form)
               (if (atom (rest form)) " . " " "))
             (maybe-print-form (form &key (prefix "") (suffix ""))
               (princ prefix ss)
               (if (listp form)
                   (print-list form)
                   (princ form ss))
               (princ suffix ss))
             (print-list (form)
               (when form
                 (maybe-print-form (first form)
                                   :prefix (first-prefix form)
                                   :suffix (first-suffix form))
                 (when (rest form)
                   (maybe-print-form (rest form) :prefix (rest-prefix form))))))
      (cond ((consp form) (princ "(" ss) (print-list form) (princ ")" ss))
            ((null form) (princ nil ss))))))

;;;;
;; Exercises 05

(defparameter *questions*
  '((:begin :animal :plant)
    (:animal :land-animal :sea-animal)
    (:land-animal :dog)
    (:sea-animal :fish)
    (:dog t)
    (:fish t)
    (:plant :tree :bush)
    (:tree :pine-tree)
    (:bush :rose-bush)
    (:pine-tree t)
    (:rose-bush t)))

(defun a-or-an-thing (key)
  "Return \"a KEY\" or \"an KEY\" (mostly) appropriately for KEY.

The result depends on a few simple (English) grammar rules so it doesn't always
work but it probably gets about 70-80% correct."
  (labels ((vowel-p (ch)
             (find ch "aeiou" :test #'char-equal)))
    (let* ((key (string key))
           (len   (length key))
           (ch1   (when (> len 0) (char key 0)))
           (ch2   (when (> len 1) (char key 1))))
      (concatenate 'string
                   (cond ((and ch1 (vowel-p ch1))                          "an ")
                         ((and ch1 ch2 (char-equal ch1 #\h) (vowel-p ch2)) "an ")
                         (t                                                "a "))
                   key))))

(defun options (key)
  "Get the options associated with KEY."
  (let ((result (cdr (assoc key *questions*))))
    (if (eql t result) (list key) result)))

(defun format-question (opt1 &optional opt2)
  "Return a string posed as a question using OPT1 and, optionally, OPT2."
  (format nil "Is it ~a~:[~; or ~a~]?" (a-or-an-thing opt1) opt2 (a-or-an-thing opt2)))

(defun question (key)
  "Generate a question for the user from KEY."
  (apply #'format-question (options key)))

(defun answer ()
  "Read an answer from the user."
  (intern (string (read)) :keyword))

(defun prompt (format-string &rest args)
  "Prompt the user for input and return it."
  (apply #'format t format-string args)
  (answer))

(defun learn-new-item (parent)
  (format t "What was it?~%> ")
  (let ((key (answer))
        (guess (first (options parent))))
    (let* ((yes (prompt "Enter two words to distinguish ~a from ~a.~%1> " guess key))
           (no  (prompt "2> "))
           (new (prompt "Does ~a apply to ~a or ~a?~%> " yes guess key)))
      (nconc *questions*
             (list (list yes (if (eql new guess) guess key)))
             (list (list no  (if (eql new guess) key guess)))
             (list (list yes t))
             (list (list no t)))
      (push (list parent yes no) *questions*))))

(defun twenty-questions (&rest questions)
  "Play a game of 20 questions with the user."
  (let ((*questions* (or questions *questions*))
        (key :begin))
    (labels ((quit-game ()
               (format t "Great, thanks for playing! Bye...~%")
               (return-from twenty-questions))
             (play-again-p ()
               (if (yes-or-no-p "Do you want to play again?")
                   (setf key :begin)
                   (quit-game))))
      (loop (format t "~a~%> " (question key))
            (let* ((tmp  (answer))
                   (opts (options tmp)))
              (cond (opts (setf key tmp))
                    ((eql tmp :quit) (quit-game))
                    ((or (eql opts t) (member tmp '(:yes :it))) (play-again-p))
                    ((eql tmp :no)
                     (learn-new-item key)
                     (play-again-p))
                    (t (format t "~%error: unrecognized key: ~a!~%~%" tmp))))))))

;;;;
;; Exercise 06

;; (setf a 'global-a)
;; (defvar *b* 'global-b)
;; (defun fn () *b*)
;; (let ((a 'local-a)
;;       (*b* 'local-b))
;;   (list a *b* (fn) (symbol-value 'a) (symbol-value '*b*)))

;; The above will return:
'(local-a local-b local-b 'global-a 'local-b)

;;;;
;; Exercise 07

;; Since it's not working for me on SBCL version 2.1.5, I couldn't say for sure
;; but, if I had to make an educated guess, I'd say it might be because
;; parameters are evaluated from left-to-right (which would make me think it
;; should apply the right-most value since it will have been the last value it
;; saw) and so once it's set, it's set because that's how it was defined in the
;; standard (or something).

;;;;
;; Exercise 08

;; see definition in: src/03/examples/examples.lisp

;;;;
;; Exercise 09

(defun length/reduce (list)
  "Calculate the length of LIST using REDUCE."
  (reduce #'(lambda (acc x)
              (declare (ignore x))
              (1+ acc))
          list))

;;;;
;; Exercise 10

;; LCM returns the least common multiple of the provided integer parameters; or
;; 1 if no parameters are provided.
;;
;; NRECONC destructively (i.e. changing the structure) returns the the
;; concatenation - i.e. NCONC - of two lists, X and Y, where the first list is
;; also destructively reversed - i.e. NREVERSE.

;;;;
;; Exercise 11

;; The function is: ACONS

;; This is the code I used to figure it out:
(let (syms)
  (do-symbols (s 'cl (reverse syms))
    (let ((doc (documentation s 'function)))
      (when (search "alist" doc :test #'string-equal)
        (push (cons s doc) syms)))))

;;;;
;; Exercise 12

;; My first version
(let ((list '(this is a sample string)))
  (format t "~:(~a~) ~{~(~a~)~^ ~}." (first list) (rest list)))

;; Once I saw the answer...
(format t "~@(~{~(~a~)~^ ~}~)." '(this is a sample string))
