(defpackage #:paip/03/examples
  (:nicknames #:03/examples #:paip.03.examples #:03.examples)
  (:use #:cl))

(in-package #:paip/03/examples)

(defun length/dolist (list)
  "Calculate the length of a LIST using DOLIST."
  (let ((len 0))
    (dolist (i list)
      (incf len))
    len))

(defun length/mapc (list)
  "Calculate the length of a LIST using MAPC."
  (let ((len 0))
    (mapc #'(lambda (i)
              (declare (ignore i))
              (incf len))
          list)
    len))

(defun length/do (list)
  "Calculate the length of a LIST using DO."
  (do ((len 0 (1+ len))
       (lst list (rest lst)))
      ((null lst) len)))

(defun length/loop/1 (list)
  "Calculate the length of a LIST using one version of LOOP."
  (loop :for i :in list :count t))

(defun length/loop/2 (list)
  "Calculate the length of a LIST using one version of LOOP."
  (loop :for i :in list :summing 1))

(defun length/loop/3 (list)
  "Calculate the length of a LIST using one version of LOOP."
  (loop :with len := 0
        :until (null list)
        :for i := (pop list)
        :do (incf len)
        :finally (return len)))

(defun length/count-if (list)
  "Calculate the length of a LIST using COUNT-IF."
  (count-if (constantly t) list))

(defun length/position-if (list)
  "Calculate the length of a LIST using POSITION-IF."
  (if (null list)
      0
      (1+ (position-if (constantly t) list :from-end t))))

(defun length/recursive (list)
  "Calculate the length of a LIST using recursion."
  (if (null list)
      0
      (1+ (length/recursive (rest list)))))

(defun length/tail-recursive (list)
  "Calculate the length of a LIST using tail recursion."
  (labels ((rec (acc list)
             (if (null list)
                 acc
                 (rec (1+ acc) (rest list)))))
    (rec 0 list)))

(defun product (list)
  "Multiply all the numbers together to compute their product."
  (let ((prod 1))
    (dolist (n list prod)
      (if (zerop n)
          (return 0)
          (setf prod (* prod n))))))

(defmacro while (test &body body)
  "Execute BODY repeatedly until TEST returns NIL."
  `(loop (unless ,test (return nil))
         ,@body))

(defun math-quiz (&key (op '+) (range 100) (n 10))
  "Ask the user a series of arithmetic problems."
  (dotimes (i n)
    (problem (random range) op (random range))))

(defun problem (x op y)
  "Ask an arithmetic problem, read a reply, and say if it is correct."
  (format t "~&How much is ~d ~a ~d? " x op y)
  (if (eql (read) (funcall op x y))
      (princ "Correct!")
      (princ "Sorry, that's not right...")))

;; FIND-ALL and FIND-ALL-IF definitions moved to src/utils.lisp
