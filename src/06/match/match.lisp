(defpackage #:paip/06/match
  (:nicknames #:06/match #:paip.06.match #:06.match #:match)
  (:use #:cl))

(in-package #:paip/06/match)

(defparameter fail nil
  "Indicates PAT-MATCH failure.")

(defparameter no-bindings '((t . t))
  "Indicates PAT-MATCH success, with no variables.")

(defun match (pattern input &optional (bindings no-bindings))
  "Match pattern against input in the context of the bindings."
  (cond ((eq bindings fail)          fail)
        ((variable-p pattern)        (match-variable pattern input bindings))
        ((eql pattern input)         bindings)
        ((segment-pattern-p pattern) (match-segment pattern input bindings))
        ((single-pattern-p pattern)  (match-single pattern input bindings))
        ((and (consp pattern) (consp input))
         (match (rest pattern) (rest input)
           (match (first pattern) (first input) bindings)))
        (t fail)))

(defun variable-p (x)
  "Is X a variable (i.e. a symbol starting with a question mark)?"
  (and (symbolp x) (char-equal (char (symbol-name x) 0) #\?)))

(defun get-binding (var bindings)
  "Find a variable/value pair in a binding list."
  (assoc var bindings))

(defun binding-var (binding)
  "Get the variable part of a single binding."
  (car binding))

(defun binding-val (binding)
  "Get the value part of a single binding."
  (cdr binding))

(defun make-binding (var val)
  "Create a new binding."
  (cons var val))

(defun lookup (var bindings)
  "Get the value part (for var) from a binding list."
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  "Add a variable/value pair to a binding list."
  (cons (make-binding var val) (if (eq bindings no-bindings) nil bindings)))

(defun match-variable (var input bindings)
  "Does VAR match INPUT? Uses (or updates) and returns BINDINGS."
  (let ((binding (get-binding var bindings)))
    (cond ((not binding) (extend-bindings var input bindings))
          ((equalp input (binding-val binding)) bindings)
          (t fail))))

(setf (get '?is  'single-match) 'match-is)
(setf (get '?or  'single-match) 'match-or)
(setf (get '?and 'single-match) 'match-and)
(setf (get '?not 'single-match) 'match-not)

(setf (get '?*  'segment-match) 'match-segment)
(setf (get '?+  'segment-match) 'match-segment+)
(setf (get '??  'segment-match) 'match-segment?)
(setf (get '?if 'segment-match) 'match-if)

(defun segment-pattern-p (pattern)
  "Is this a segment-matching pattern like ((?* VAR) . PAT)?"
  (and (consp pattern) (consp (first pattern))
       (symbolp (first (first pattern)))
       (match-segment-fn (first (first pattern)))))

(defun single-pattern-p (pattern)
  "Is this a single-matching pattern?
E.g. (?IS X PREDICATE) (?AND . PATTERNS) (?OR . PATTERNS)."
  (and (consp pattern)
       (match-single-fn (first pattern))))

(defun segment-match (pattern input bindings)
  "Call the right function for this kind of segment pattern."
  (funcall (match-segment-fn (first (first pattern)))
           pattern input bindings))

(defun match-single (pattern input bindings)
  "Call the right function for this kind of single pattern."
  (funcall (match-single-fn (first pattern))
           (rest pattern) input bindings))

(defun match-segment-fn (x)
  "Get the segment-match function for X if it is a symbol that has one."
  (when (symbolp x) (get x 'segment-match)))

(defun match-single-fn (x)
  "Get the single-match function for X if it is a symbol that has one."
  (when (symbolp x) (get x 'single-match)))

(defun match-is (var-and-pred input bindings)
  "Succeed and bind var if the INPUT satisfies pred, where VAR-AND-PRED is the
list (VAR PRED)."
  (let* ((var  (first var-and-pred))
         (pred (second var-and-pred))
         (new-bindings (match var input bindings)))
    (if (or (eq new-bindings fail) (not (funcall pred input)))
        fail
        new-bindings)))

(defun match-or (patterns input bindings)
  "Succeed if any of the PATTERNS match the INPUT."
  (if patterns
      (let ((new-bindings (match (first patterns) input bindings)))
        (if (eq new-bindings fail)
            (match-or (rest patterns) input bindings)
            new-bindings))
      fail))

(defun match-and (patterns input bindings)
  "Succeed if all of the PATTERNS match the INPUT."
  (cond ((eq bindings fail) fail)
        ((null patterns)    bindings)
        (t (match-and (rest patterns) input bindings))))

(defun match-not (patterns input bindings)
  "Succeed if none of the PATTERNS match the INPUT. This will never bind any
variables."
  (if (match-or patterns input bindings)
      fail
      bindings))

(defun match-segment (pattern input bindings &optional (start 0))
  "Match the segment PATTERN ((?* VAR) . PAT) against INPUT."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (if pat
        (let ((pos (first-match-pos (first pat) input start)))
          (if pos
              (let* ((var (match-variable var (subseq input 0 pos) bindings))
                     (b2  (match pat (subseq input pos) var)))
                (if (eq b2 fail)
                    (match-segment pattern input bindings (1+ pos))
                    b2))
              fail))
        (match-variable var input bindings))))

(defun first-match-pos (pattern input start)
  "Find the first position that PATTERN could possibly match INPUT starting at
position START. If PATTERN is non-constant, then just return START."
  (cond ((and (atom pattern) (not (variable-p pattern)))
         (position pattern input :start start :test #'equal))
        ((< start (length input)) start)
        (t nil)))

(defun match-segment+ (pattern input bindings)
  "Match one or more elements of INPUT."
  (match-segment pattern input bindings 1))

(defun match-segment? (pattern input bindings)
  "Match zero or one element of INPUT."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (or (match (cons var pat) input bindings)
        (match pat input bindings))))

(defun match-if (pattern input bindings)
  "Test an arbitrary expression involving variables. The pattern looks like
((?IF CODE) . REST)."
  (and (progv (mapcar #'car bindings) (mapcar #'cdr bindings)
         (eval (second (first pattern))))
       (match (rest pattern) input bindings)))

(defun match-abbrev (symbol expansion)
  "Define SYMBOL as a macro standing for a MATCH pattern."
  (setf (get symbol 'expand-match-abbrev)
        (expand-match-abbrev expansion)))

(defun expand-match-abbrev (pattern)
  "Expand out all pattern matching abbreviations in PATTERN."
  (cond ((and (symbolp pattern) (get pattern 'expand-match-abbrev)))
        ((atom pattern) pattern)
        (t (cons (expand-match-abbrev (first pattern))
                 (expand-match-abbrev (rest pattern))))))
