(defpackage #:paip/06/interpreter
  (:nicknames #:06/interpreter #:paip.06.interpreter #:06.interpreter #:interpreter)
  (:use #:cl))

(in-package #:paip/06/interpreter)

(defun interactive-interpreter (prompt transformer)
  "Read an expression, transform it, and print the result."
  (loop
    (handler-case (progn
                    (if (stringp prompt) (print prompt) (funcall prompt))
                    (print (funcall transformer (read))))
      (error (condition)
        (format t "~&;; Error ~a ignored, back to top level." condition)))))

(defun prompt-generator (&optional (num 0) (ctl-string "[~d] "))
  "Return a function that prints prompts like [1], [2], etc."
  #'(lambda () (format t ctl-string (incf num))))
