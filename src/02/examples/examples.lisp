(defpackage #:paip/02/examples
  (:nicknames #:02/examples #:paip.02.examples #:02.examples)
  (:use #:cl)
  (:import-from #:paip/utils
                #:mappend
                #:cross-product
                #:random-elt))

(in-package #:paip/02/examples)

(defparameter *simple-grammar*
  '((:sentence    -> (:noun-phrase :verb-phrase))
    (:noun-phrase -> (:article :noun))
    (:verb-phrase -> (:verb :noun-phrase))
    (:article     -> :the :a)
    (:noun        -> :man :ball :woman :table)
    (:verb        -> :hit :took :saw :liked))
  "A grammar for a trivial subset of English.")

(defparameter *bigger-grammar*
  '((:sentence    -> (:noun-phrase :verb-phrase))
    (:noun-phrase -> (:article :adj* :noun :pp*) (:name) (:pronoun))
    (:verb-phrase -> (:verb :noun-phrase :pp*))
    (:pp*         -> () (:pp :pp*))
    (:adj*        -> () (:adj :adj*))
    (:pp          -> (:prep :noun-phrase))
    (:prep        -> :to :in :by :with :on)
    (:adj         -> :big :little :blue :green :adiabatic)
    (:article     -> :the :a)
    (:name        -> :pat :kim :lee :terry :robin)
    (:noun        -> :man :ball :woman :table)
    (:verb        -> :hit :took :saw :liked)
    (:pronoun     -> :he :she :it :these :those :that)))

(defvar *grammar* *simple-grammar*
  "The grammar used by GENERATE. Initially, this is *SIMPLE-GRAMMAR*, but we can
switch to other grammars.")

(defun one-of (choices)
  "Pick one element of a set of CHOICES and make a list with it."
  (list (random-elt choices)))

(defun rule-lhs (rule)
  "The left-hand-side of a RULE."
  (first rule))

(defun rule-rhs (rule)
  "The right-hand-side of a RULE."
  (rest (rest rule)))

(defun rewrites (category)
  "Return a list of the possible rewrites for this CATEGORY."
  (rule-rhs (assoc category *grammar*)))

(defun generate (phrase)
  "Generate a random sentence or phrase."
  (cond ((listp phrase)    (mappend #'generate phrase))
        ((rewrites phrase) (generate (random-elt (rewrites phrase))))
        (t                 (list phrase))))

(defun generate-tree (phrase)
  "Generate a random sentence or phrase, with a complete parse-tree."
  (cond ((listp phrase)    (mapcar #'generate-tree phrase))
        ((rewrites phrase) (cons phrase (generate-tree (random-elt (rewrites phrase)))))
        (t                 (list phrase))))

(defun generate-all (phrase)
  "Generate a list of all possible expansions of this phrase."
  (cond ((null phrase)     (list nil))
        ((listp phrase)    (combine-all (generate-all (first phrase))
                                        (generate-all (rest phrase))))
        ((rewrites phrase) (mappend #'generate-all (rewrites phrase)))
        (t                 (list (list phrase)))))

(defun combine-all (xlist ylist)
  "Return a list of lists formed by appending a Y to an X.

E.g. (combine-all '((a) (b)) '((1) (2)))
     -> ((A 1) (B 1) (A 2) (B 2))"
  (cross-product #'append xlist ylist))
