(defpackage #:paip/02/exercises
  (:nicknames #:02/exercises #:paip.02.exercises #:02.exercises)
  (:use #:cl)
  (:import-from #:paip/utils
                #:mappend)
  (:import-from #:paip/02/examples
                #:*grammar*
                #:rewrites
                #:random-elt
                #:generate))

(in-package #:paip/02/exercises)

;;;;
;; Exercise 01

(defun generate/2 (phrase)
  "Generate a random sentence or phrase."
  (let (choices)
    (cond ((listp phrase)                   (mappend #'generate/2 phrase))
          ((setf choices (rewrites phrase)) (generate/2 (random-elt choices)))
          (t                                (list phrase)))))

;;;;
;; Exercise 02

(defun generate/3 (phrase)
  "Generate a random sentence or phrase."
  (cond ((listp phrase)          (mappend #'generate/3 phrase))
        ((non-terminal-p phrase) (generate/3 (random-elt (rewrites phrase))))
        (t                       (list phrase))))

(defun non-terminal-p (category)
  "Return non-NIL if CATEGORY is a non-terminal; NIL, otherwise."
  (rewrites category))

;;;;
;; Exercise 03

(defparameter *min-lisp-grammar*
  '((:expression -> (#\( :operator :argument* #\)))
    (:argument*  -> () (:space :argument) (:space :argument :argument*))
    (:argument   -> (:literal :literal*))
    (:literal*   -> () (:literal :literal*))
    (:literal    -> (:lst) (:number))
    (:lst        -> (:nil) (#\( :list :argument* #\)))
    (:operator   -> :list :quote)
    (:number     -> (:sign? :digit :digit*))
    (:digit*     -> () (:digit :digit*))
    (:digit      -> 0 1 2 3 4 5 6 7 8 9)
    (:sign?      -> () (:+) (:-))
    (:space      -> #\Space)))

(let ((*grammar* *min-lisp-grammar*))
  (generate :expression))

;;;;
;; Exercise 04

;; moved definition to: src/utils.lisp
