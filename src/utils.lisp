(defpackage #:paip/utils
  (:nicknames #:paip.utils #:utils)
  (:use #:cl)
  (:export #:mappend
           #:last1
           #:flatten
           #:cross-product
           #:find-all-if
           #:find-all
           #:starts-with
           #:random-elt
           #:whitespace-p
           #:punctuation-p
           #:string-empty-p))

(in-package #:paip/utils)

(defun mappend (fn list)
  "Apply the function FN to each element of the LIST and append the results."
  (apply #'append (mapcar fn list)))

(defun last1 (list)
  "Return the last item from LIST."
  (first (last list)))

(defun flatten (form)
  "Flatten a FORM (i.e. remove all nested expressions)."
  (labels ((rec (acc form)
             (cond ((null form) acc)
                   ((atom form) (append acc (list form)))
                   (t (append (rec acc (first form))
                              (append (rec acc (rest form)) acc))))))
    (rec nil form)))

;;;;
;; Exercise 2.4

(defun cross-product (fn xlist ylist)
  "Return the cross-product of XLIST and YLIST."
  (mappend #'(lambda (y)
               (mapcar #'(lambda (x) (funcall fn x y)) xlist))
           ylist))

(setf (symbol-function 'find-all-if) #'remove-if-not)

(defun find-all (item sequence &rest args
                 &key from-end (test #'eql) test-not (start 0) end count key)
  "Return a list of every ITEM in SEQUENCE that satisfies TEST."
  (declare (ignore from-end start end count key))
  (labels ((modify-args (keyword test)
             (list* keyword (funcall #'complement test)
                    (loop :for k :in args :by #'cddr
                          :and v :in (rest args) :by #'cddr
                          :unless (eql keyword k)
                            :collect k :and :collect v))))
    (apply #'remove item sequence
           (apply #'modify-args
                  (if test-not (list :test-not test-not) (list :test test))))))

(defun starts-with (list x)
  "Is this a LIST whose first element is X?"
  (and (consp list) (string-equal (string (first list)) x)))

(defun random-elt (choices)
  "Choose an element from a set of CHOICES at random."
  (elt choices (random (length choices))))

(defun whitespace-p (ch)
  "Return CH if it is a whitespace character; NIL, otherwise."
  (first (member ch '(#\Space #\Tab #\Newline))))

(defun punctuation-p (ch)
  "Return CH if it is a punctuation character; NIL, otherwise."
  (and (find ch "!`',.;:-()&#<>?\"\\") ch))

(defun string-empty-p (str)
  "Return T if STR is empty; NIL, otherwise."
  (string= str ""))
