(defpackage #:paip/04/dbg
  (:nicknames #:04/dbg #:paip.04.dbg #:dbg)
  (:use #:cl))

(in-package #:paip/04/dbg)

(defvar *dbg-ids* nil "Identifiers used by DBG.")

;;;;
;; Exercise 4.1

(defun dbg (id format-string &rest args)
  "Print debugging output if (DEBUG ID) has been specified."
  (format *debug-io* "~:[~;~&~?~]" (member id *dbg-ids*) format-string args))

;; Renaming to DEBUG* to avoid package locks
(defun debug* (&rest ids)
  "Start DBG output on the given IDS."
  (setf *dbg-ids* (union ids *dbg-ids*)))

;; Renaming to UNDEBUG* to be consistent with DEBUG*
(defun undebug* (&rest ids)
  "Stop DBG output on the given IDS. With no arguments, stop DBG output on all
IDS."
  (setf *dbg-ids* (if (null ids) nil (set-difference *dbg-ids* ids))))

(defun dbg-indent (id indent format-string &rest args)
  "Print indented debugging information if (DEBUG* ID) has been specified."
  (format *debug-io* "~:[~;~&~v@t~?~]"
         (member id *dbg-ids*) (* 2 indent) format-string args))
