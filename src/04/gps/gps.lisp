(defpackage #:paip/04/gps
  (:nicknames #:04/gps #:paip.04.gps #:gps)
  (:use #:cl)
  (:import-from #:paip/04/dbg
                #:dbg-indent)
  (:import-from #:paip/utils
                #:mappend
                #:find-all-if
                #:find-all
                #:starts-with))

(in-package #:paip/04/gps)

(defvar *ops* nil "A list of available operators.")

(defstruct op "An operation"
  action preconditions add-list delete-list)

(defun gps (state goals &optional (*ops* *ops*))
  "General Problem Solver: From STATE achieve all goals using *OPS*."
  (find-all-if #'action-p (achieve-all (cons '(start) state) goals nil)))

(defun action-p (x)
  "Is X something that is (START) or (EXECUTING ...)?"
  (or (equal x '(start)) (executing-p x)))

(defun achieve (state goal goal-stack)
  "A goal is achieved if it already holds, or if there is an appropriate OP for
it that is applicable."
  (dbg-indent :gps (length goal-stack) "Goal: ~a" goal)
  (cond ((member-equal goal state)      state)
        ((member-equal goal goal-stack) nil)
        (t (some #'(lambda (op) (apply-op state goal op goal-stack))
                 (appropriate-ops goal state)))))

(defun appropriate-ops (goal state)
  "Return a list of appropriate operators sorted by the number of unfulfilled
preconditions."
  (let ((operations (find-all goal *ops* :test #'appropriate-p)))
    (sort (copy-list operations) #'<
          :key #'(lambda (op)
                   (count-if #'(lambda (p)
                                 (and (not (member-equal p state))
                                      (notany #'(lambda (o)
                                                  (member-equal o (op-add-list op)))
                                              (first (mapcar #'op-preconditions
                                                             (remove op operations))))))
                             (op-preconditions op))))))

(defun appropriate-p (goal op)
  "An OP is appropriate to a goal if it is in its ADD-LIST."
  (member-equal goal (op-add-list op)))

(defun apply-op (state goal op goal-stack)
  "Return a new, transformed state, if OP is applicable."
  (dbg-indent :gps (length goal-stack) "Consider: ~a" (op-action op))
  (let ((state2 (achieve-all state (op-preconditions op) (cons goal goal-stack))))
    (unless (null state2)
      (dbg-indent :gps (length goal-stack) "Action: ~a" (op-action op))
      (append (remove-if #'(lambda (x) (member-equal x (op-delete-list op))) state2)
              (op-add-list op)))))

(defun achieve-all (state goals goal-stack)
  "Achieve each goal, trying several orderings."
  (some #'(lambda (goals) (achieve-each state goals goal-stack))
        (orderings goals)))

(defun achieve-each (state goals goal-stack)
  "Achieve each goal, and make sure they still hold at the end."
  (let ((current-state state))
    (if (and (every #'(lambda (g)
                        (setf current-state (achieve current-state g goal-stack)))
                    goals)
             (subsetp goals current-state :test #'equal))
        current-state)))

(defun orderings (goals)
  (if (> (length goals) 1)
      (list goals (reverse goals))
      (list goals)))

(defun executing-p (x)
  "Is X of the form (EXECUTING ...)?"
  (starts-with x 'executing))

(defun convert-op (op)
  "Make OP conform to the (EXECUTING OP) convention."
  (unless (some #'executing-p (op-add-list op))
    ;; So why are we adding it to OP-ADD-LIST?
    (push (list 'executing (op-action op)) (op-add-list op))
    op))

(defun op (action &key preconditions add-list delete-list)
  "Make a new operator that obeys the (EXECUTING op) convention"
  (convert-op (make-op :action action
                       :preconditions preconditions
                       :add-list add-list
                       :delete-list delete-list)))

(defun member-equal (item list)
  (member item list :test #'equal))

(defun use (oplist)
  "Use OPLIST as the default list of operators and return the number of used
operators."
  (length (setf *ops* oplist)))

(defparameter *school-ops*
  (list
   (make-op :action        'drive-son-to-school
            :preconditions '(son-at-home car-works)
            :add-list      '(son-at-school)
            :delete-list   '(son-at-home))
   (make-op :action        'shop-installs-battery
            :preconditions '(car-needs-battery shop-knows-problem shop-has-money)
            :add-list      '(car-works))
   (make-op :action        'tell-shop-problem
            :preconditions '(in-communication-with-shop)
            :add-list      '(shop-knows-problem))
   (make-op :action        'telephone-shop
            :preconditions '(know-phone-number)
            :add-list      '(in-communication-with-shop))
   (make-op :action        'lookup-number
            :preconditions '(have-phone-book)
            :add-list      '(know-phone-number))
   (make-op :action        'give-shop-money
            :preconditions '(have-money)
            :add-list      '(shop-has-money)
            :delete-list   '(have-money))))

(push (make-op :action        'ask-phone-number
               :preconditions '(in-communication-with-shop)
               :add-list      '(know-phone-number))
      *school-ops*)

(mapc #'convert-op *school-ops*)

(use *school-ops*)

(gps '(son-at-home car-needs-battery have-money have-phone-book)
     '(son-at-school))

(gps '(son-at-home car-needs-battery have-money)
     '(son-at-school))

(gps '(son-at-home car-works)
     '(son-at-school))

;; The "Prerequisite clobbers sibling goal" problem
(gps '(son-at-home have-money car-works)
     '(have-money son-at-school))

(gps '(son-at-home car-needs-battery have-money have-phone-book)
     '(have-money son-at-school))

;; The "Leaping before you look" problem
(gps '(son-at-home car-needs-battery have-money have-phone-book)
     '(have-money son-at-school))

;; The "Recursive subgoal" problem
(gps '(son-at-home car-needs-battery have-money)
     '(son-at-school))

;;;;
;; Monkey/bananas

(defparameter *banana-ops*
  (list
   (op 'climb-on-chair
       :preconditions '(chair-at-middle-room at-middle-room on-floor)
       :add-list      '(at-bananas on-chair)
       :delete-list   '(at-middle-room on-floor))
   (op 'push-chair-from-door-to-middle-room
       :preconditions '(chair-at-door at-door)
       :add-list      '(chair-at-middle-room at-middle-room)
       :delete-list   '(chair-at-door at-door))
   (op 'walk-from-door-to-middle-room
       :preconditions '(at-door on-floor)
       :add-list      '(at-middle-room)
       :delete-list   '(at-door))
   (op 'grasp-bananas
       :preconditions '(at-bananas empty-handed)
       :add-list      '(has-bananas)
       :delete-list   '(empty-handed))
   (op 'drop-ball
       :preconditions '(has-ball)
       :add-list      '(empty-handed)
       :delete-list   '(has-ball))
   (op 'eat-bananas
       :preconditions '(has-bananas)
       :add-list      '(empty-handed not-hungry)
       :delete-list   '(has-bananas hungry))))

(use *banana-ops*)

(gps '(at-door on-floor has-ball hungry chair-at-door) '(not-hungry))

;;;;
;; Mazes

(defun make-maze-ops (pair)
  "Make maze operations in both directions."
  (list (make-maze-op (first pair) (second pair))
        (make-maze-op (second pair) (first pair))))

(defun make-maze-op (here there)
  "Make an operator to move between HERE and THERE."
  (op `(move from ,here to ,there)
      :preconditions `((at ,here))
      :add-list      `((at ,there))
      :delete-list   `((at ,here))))

(defparameter *maze-ops*
  (mappend #'make-maze-ops
           '((1 2)   (2 3)   (3 4)   (4 9)   (9 14)  (9 8)   (8 7)   (7 12)
             (12 13) (12 11) (11 6)  (11 16) (16 17) (17 22) (21 22) (22 23)
             (23 18) (23 24) (24 19) (19 20) (20 15) (15 10) (10 5)  (20 25))))

(use *maze-ops*)

(defun find-path (start end)
  "Search a maze for a path from START to END."
  (let ((results (gps `((at ,start)) `((at ,end)))))
    (when results
      (cons start (mapcar #'destination (remove '(start) results :test #'equal))))))

(defun destination (action)
  "Find the Y in (EXECUTING (MOVE FROM X TO Y))."
  (fifth (second action)))

(find-path 1 25)
(find-path 1 1)
(equal (find-path 1 25) (reverse (find-path 25 1)))

;;;;
;; Block world

(defun make-block-ops (blocks)
  (let (ops)
    (dolist (a blocks)
      (dolist (b blocks)
        (unless (equal a b)
          (dolist (c blocks)
            (unless (or (equal c a) (equal c b))
              (push (move-block a b c) ops)))
          (push (move-block a 'table b) ops)
          (push (move-block a b 'table) ops))))
    ops))

(defun move-block (a b c)
  "Make an operator to move A from B to C."
  (op `(move ,a from ,b to ,c)
      :preconditions `((space on ,a) (space on ,c) (,a on ,b))
      :add-list      (move-ons a b c)
      :delete-list   (move-ons a c b)))

(defun move-ons (a b c)
  (if (eq b 'table)
      `((,a on ,c))
      `((,a on ,c) (space on ,b))))

(use (make-block-ops '(a b c)))

(gps '((c on a) (a on table) (b on table) (space on c) (space on b) (space on table))
     '((c on table)))

(gps '((c on a) (a on table) (b on table) (space on c) (space on b) (space on table))
     '((c on table) (a on b)))

(gps '((a on b) (b on c) (c on table) (space on a) (space on table))
     '((b on a) (c on b)))

(let ((start '((c on a) (a on table) (b on table)
               (space on c) (space on b) (space on table))))
  (gps start '((a on b) (b on c)))
  (gps start '((b on c) (a on b))))

(use (push (op 'taxi-son-to-school
               :preconditions '(son-at-home have-money)
               :add-list      '(son-at-school)
               :delete-list   '(son-at-home have-money))
           *school-ops*))

(gps '(son-at-home have-money car-works)
     '(son-at-school have-money))
