(defpackage #:paip/04/exercises
  (:nicknames #:04/exercises #:paip.04.exercises #:04.exercises)
  (:use #:cl)
  (:import-from #:paip/04/gps
                #:gps
                #:op
                #:use))

(in-package #:paip/04/exercises)

;;;;
;; Exercise 01

;; See implementation in src/04/dbg.lisp

;;;;
;; Exercise 02

;; I couldn't figure this one out so I had to look at the answer. That said,
;; I've walked through it and I think I have a pretty good understanding of how
;; it works.

(defun permute (&rest args)
  "Return a list of all permutations of each input argument."
  (if args
      (mapcan #'(lambda (a)
                  (mapcar #'(lambda (p) (cons a p))
                          (apply #'permute (remove a args :count 1 :test #'eq))))
              args)
      '(())))

;;;;
;; Exercise 03

;; * Part 01

(defparameter *dessert-ops*
  (list
   (op 'gps::get-ice-cream
       :preconditions '(gps::cake-eaten)
       :add-list      '(gps::have-ice-cream))
   (op 'gps::get-cake
       :preconditions '(gps::have-money)
       :add-list      '(gps::have-cake)
       :delete-list   '(gps::have-money))
   (op 'gps::eat-ice-cream
       :preconditions '(gps::have-ice-cream)
       :add-list      '(gps::dessert-eaten gps::ice-cream-eaten)
       :delete-list   '(gps::have-ice-cream))
   (op 'gps::eat-cake
       :preconditions '(gps::have-cake)
       :add-list      '(gps::dessert-eaten gps::cake-eaten gps::have-ice-cream)
       :delete-list   '(gps::have-cake))))

;; * Part 02

(use *dessert-ops*)

(gps '(have-money) '(dessert-eaten))

;; * Part 03

;; See src/04/gps/gps.lisp
;; The solution I came up with was to update `appropriate-ops' so that it would
;; look at the add list of other appropriate operations and prioritize those
;; operations that don't have preconditions in another operation's add-list.

;;;;
;; Exercise 04

;; I'm going to have to come back to this one once I have some more experience.

;;;;
;; Exercise 05

;; I'm going to have to come back to this one once I have some more experience.

;;;;
;; Exercise 06

;; I'm going to have to come back to this one once I have some more experience.

;;;;
;; Exercise 07

;; I'm going to have to come back to this one once I have some more experience.
