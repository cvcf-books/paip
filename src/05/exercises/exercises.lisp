(defpackage #:paip/05/exercises
  (:nicknames #:05.exercises #:paip.05.exercises #:05.exercises)
  (:use #:cl))

(in-package #:paip/05/exercises)

;;;;
;; Exercise 01

;; I'd say it would be better to use the simpler `every' form instead of using
;; `and' because it is more obvious what's happening.
;;
;; That said, after seeing the answer, it brings up a good point about not
;; handling it correctly when the size of the lists are different or if one, or
;; both, lists are a dotted list.

;;;;
;; Exercise 02

;; An example of it working fairly well:
;;
;; ELIZA> (hello)
;; (HOW DO YOU DO. PLEASE STATE YOUR PROBLEM.)
;; ELIZA> (i want to build a cabin in the woods so i can get a change of scenery)
;; (SUPPOSE YOU GOT TO BUILD A CABIN IN THE WOODS SO YOU CAN GET A CHANGE OF SCENERY SOON)
;; ELIZA> (that would be great! i feel like i could relax in that case.)
;; (DO YOU OFTEN FEEL LIKE YOU COULD RELAX IN THAT CASE. ?)
;; ELIZA> (yes i felt like that just yesterday)
;; (WHAT OTHER FEELINGS DO YOU HAVE?)
;; ELIZA> (i feel like the world is going crazy)
;; (DO YOU OFTEN FEEL LIKE THE WORLD IS GOING CRAZY ?)
;; ELIZA> (no not until recently)
;; (ARE YOU SAYING "NO" JUST TO BE NEGATIVE?)
;; ELIZA> (no! i am saying it because i feel like it actually is going crazy)
;; (YOU ARE BEING A BIT NEGATIVE)
;; ELIZA> (i was actually being observant)
;; (PERHAPS I ALREADY KNEW YOU WERE ACTUALLY BEING OBSERVANT)
;; ELIZA> (well why not say so if you already knew)
;; (DO YOU REALLY THINK IT IS LIKELY THAT I ALREADY KNEW ?)
;; ELIZA> (i guess i felt like you did)
;; (WHAT OTHER FEELINGS DO YOU HAVE?)
;; ELIZA> (i feel like it is time to go)
;; (DO YOU OFTEN FEEL LIKE IT IS TIME TO GO ?)
;; ELIZA> (no but when i do then i go)
;; (YOU ARE BEING A BIT NEGATIVE)
;; ELIZA> [abort]

;; An example where it doesn't work that well (unless this is a horrible
;; therapist):
;;
;; ELIZA> (hello there)
;; (HOW DO YOU DO. PLEASE STATE YOUR PROBLEM.)
;; ELIZA> (my problem? i do not have any problem)
;; NIL
;; ELIZA> (what is your problem?)
;; NIL
;; ELIZA> (yeah that is what i thought-- bye)
;; NIL
;; ELIZA> (i feel like i am invisible to you!)
;; (DO YOU OFTEN FEEL LIKE YOU ARE INVISIBLE TO YOU! ?)
;; ELIZA> (no i am invisible to YOU!)
;; (ARE YOU SAYING "NO" JUST TO BE NEGATIVE?)
;; ELIZA> (how dare you? i was just pointing out the obvious)
;; (WHY DO YOU TELL ME YOU WERE JUST POINTING OUT THE OBVIOUS NOW?)
;; ELIZA> (because you did not answer me and i was feeling ignored)
;; (PERHAPS I ALREADY KNEW YOU WERE FEELING IGNORED)
;; ELIZA> (so you were doing it on purpose?)
;; NIL
;; ELIZA> (here we go again! i feel like it is time for me to go)
;; (DO YOU OFTEN FEEL LIKE IT IS TIME FOR YOU TO GO ?)
;; ELIZA> (what now you answer?! you are the worst therapist ever!)
;; NIL
;; ELIZA> [abort]

;; Another example where it doesn't work that well (unless this is a horrible
;; therapist):
;;
;; ELIZA> (hello there)
;; (HOW DO YOU DO. PLEASE STATE YOUR PROBLEM.)
;; ELIZA> (i want to be a good parent)
;; (WHY DO YOU WANT TO BE A GOOD PARENT ?)
;; ELIZA> (because then i feel like i will have a good relationship with my children)
;; (DO YOU OFTEN FEEL LIKE YOU WILL HAVE A GOOD RELATIONSHIP WITH MY CHILDREN ?)
;; ELIZA> (um no -- i do not have any relationship with your children)
;; (ARE YOU SAYING "NO" JUST TO BE NEGATIVE?)
;; ELIZA> (do you know something i do not?)
;; NIL
;; ELIZA> (well i feel like this has gotten off topic)
;; (DO YOU OFTEN FEEL LIKE THIS HAS GOTTEN OFF TOPIC ?)
;; ELIZA> (come to think of it i was just thinking this tends to happen a lot)
;; (WHY DO YOU TELL ME YOU WERE JUST THINKING THIS TENDS TO HAPPEN A LOT NOW?)
;; ELIZA> (well last week you said i am invisible to myself)
;; NIL
;; ELIZA> (oh great-- here we go again. i was just talking to you)
;; (WERE YOU REALLY?)
;; ELIZA> (yes you didn't hear me say that?  i guess i am invisible)
;; NIL
;; ELIZA> (bye)
;; NIL
;; ELIZA> [abort]

;; A few things can be observed about the failures:
;; 1. The rule set is not very comprehensive so we cannot handle affirmative
;;    answers - for example, a "yes" response instead of a "no" repsonse.
;; 2. Unrecongnized answers just return NIL which isn't necessarily meaningful
;;    to a non-programmer.
;; 3. The viewpoint switching doesn't always work as observed by a couple of
;;    responses in the last couple interactions.
;;
;; The rule set could be expanded to recongize more statements, to recongize
;; questions, to handle variations on the grammatical tense being used, etc.
;;
;; Failures 1 and 3 could be alleviated by expanding the rule set.
;; Failure 3 could be alleviated by updating eliza to return a different
;; response on failure rather than `NIL'.
;; Additionally, another option would be to update `pat-match' to allow using
;; the same rule for different cases (e.g. recognize `hello' and `hi' using the
;; same rule).

;;;;
;; Exercise 03

(defparameter *tracy-rules*
  '((((?* ?x) hello (?* ?y))
     (hello! which destination are you interested visiting?))
    (((?* ?x) most popular destination (?* ?y))
     (we actually have been getting a lot of interest in alaska!))
    (((?* ?x) go to (?* ?y))
     (you are in luck-- we have a great package on trips to ?y !)
     (what is in ?y that interested you in going there?)
     (what will you do in ?y ?)
     (how did you hear about ?y ?))
    (((?* ?x) how much (?* ?y))
     (the cost for the trip varies based on your travel package)
     (how much would a trip be worth to you?))
    (((?* ?x) how many days (?* ?y))
     (all our packages come with a minimum of 5 days)
     (for how many days were you looking to get away?))
    (((?* ?x) yes (?* ?y))
     (how will you be paying-- visa or mastercard?)
     (will you be paying for that by cash or check?))
    (((?* ?x) no (?* ?y))
     (are you sure? this price will be gone soon!)
     (well would you like to reserve your spot and - if you change your mind -
      you can use our cancellation policy.))
    (((?* ?x) look elsewhere (?* ?y))
     (well what is it that makes you want to look elsewhere?)
     (we are the only game in town sweet-cheeks.))
    (((?* ?x) what packages (?* ?y))
     (we have packages for anywhere in the world-- where do you want to go?)
     (which kind of packages are you looking for?)
     (we have everything from a getaway in the woods to a tropical beach resort.
      what sounds good?))
    (((?* ?x) cold (?* ?y))
     (you should check out denmark!)
     (you can always visit alaska!)
     (do you want to make friends with penguins or polar bears?))
    (((?* ?x) hot (?* ?y))
     (we have a great package for rio de janeiro in brazil?)
     (what about visiting to navagio in greece?))
    (((?* ?x) bye (?* ?y))
     (okay bye bye! come back soon!))))

;; Uncomment to run using `*tracy-rules*'.
;; (eliza::eliza *tracy-rules*)

;;;;
;; Exercise 04

;; The question says we can't use double-quotes but that doesn't seem to be an
;; issue. Single quotes, on the other hand, do pose an issue so I'll answer
;; assuming the question *should* have said "single quotes and commas".
;;
;; We cannot use single quotes or commas in the input or output because they
;; mean something specific to the `read' function. In the case of a single
;; quote, `QUOTE' will be displayed in place of the actual single quote
;; character. In the case of a comma, a runtime error will occur since it's not
;; inside a backquoted expression.

;;;;
;; Exercise 05

;; See src/05/eliza/eliza.lisp

;;;;
;; Exercise 06

;; See src/05/eliza/eliza.lisp
