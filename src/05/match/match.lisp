(defpackage #:paip/05/match
  (:nicknames #:05/match #:paip.05.match #:05.match)
  (:use #:cl)
  (:import-from #:paip/utils
                #:starts-with))

(in-package #:paip/05/match)

(defparameter fail nil
  "Indicates PAT-MATCH failure.")

(defparameter no-bindings '((t . t))
  "Indicates PAT-MATCH success, with no variables.")

(defun pat-match (pattern input &optional (bindings no-bindings))
  "Does PATTERN match INPUT? Any variable can match anything."
  (cond ((eq bindings fail)          fail)
        ((variable-p pattern)        (match-variable pattern input bindings))
        ((eql pattern input)         bindings)
        ((segment-pattern-p pattern) (match-segment pattern input bindings))
        ((and (consp pattern) (consp input))
         (pat-match (rest pattern) (rest input)
                    (pat-match (first pattern) (first input) bindings)))
        (t fail)))

(defun match-variable (var input bindings)
  "Does VAR match INPUT? Uses (or updates) and returns BINDINGS."
  (let ((binding (get-binding var bindings)))
    (cond ((not binding) (extend-bindings var input bindings))
          ((equalp input (binding-val binding)) bindings)
          (t fail))))

(defun match-segment (pattern input bindings)
  (labels ((rec (pattern input bindings start)
             (let ((var (second (first pattern)))
                   (pat (rest pattern)))
               (if (null pat)
                   (match-variable var input bindings)
                   (let ((pos (position (first pat) input
                                        :start start :test #'equalp)))
                     (if (null pos)
                         fail
                         (let ((b2 (pat-match
                                    pat (subseq input pos)
                                    (match-variable var (subseq input 0 pos) bindings))))
                           (if (eq b2 fail)
                               (rec pattern input bindings (1+ pos))
                               (match-variable var (subseq input 0 pos) b2)))))))))
    (rec pattern input bindings 0)))

(defun variable-p (x)
  "Is X a variable (i.e. a symbol starting with a question mark)?"
  (and (symbolp x) (char-equal (char (symbol-name x) 0) #\?)))

(defun segment-pattern-p (x)
  "Is this a segment matching pattern: ((?* var) . pattern)."
  (and (consp x) (starts-with (first x) '?*)))

(defun get-binding (var bindings)
  "Find a variable/value pair in a binding list."
  (assoc var bindings))

(defun binding-val (binding)
  "Get the value part of a single binding"
  (cdr binding))

(defun lookup (var bindings)
  "Get the value part (for var) from a binding list."
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  "Add a variable/value pair to a binding list."
  (cons (cons var val) (if (eq bindings no-bindings) nil bindings)))
