(defpackage #:paip/05/eliza
  (:nicknames #:05/eliza #:paip.05.eliza #:05.eliza #:eliza)
  (:use #:cl)
  (:import-from #:paip/utils
                #:flatten
                #:random-elt
                #:whitespace-p
                #:punctuation-p
                #:string-empty-p)
  (:import-from #:paip/06/match
                #:match
                #:match-abbrev
                #:fail))

(in-package #:paip/05/eliza)

(match-abbrev '?x* '(?* ?x))
(match-abbrev '?y* '(?* ?y))

(defparameter *eliza-rules*
  '(((?x* hello ?y*)
     (how do you do. please state your problem.))
    ((?x* i want ?y*)
     (what would it mean if you got ?y ?)
     (why do you want ?y ?)
     (suppose you got ?y soon))
    ((?x* if ?y*)
     (do you really think it is likely that ?y ?)
     (do you wish that ?y ?)
     (what do you think about ?y ?)
     (really-- if ?y))
    ((?x* no ?y*)
     (why not?)
     (you are being a bit negative)
     (are you saying "NO" just to be negative?))
    ((?x* i was ?y*)
     (were you really?)
     (perhaps i already knew you were ?y)
     (why do you tell me you were ?y now?))
    ((?x* i feel ?y*)
     (do you often feel ?y ?))
    ((?x* i felt ?y*)
     (what other feelings do you have?))))
(defparameter *exit-string* "bye")


(defun rule-pattern (rule) (first rule))
(defun rule-responses (rule) (rest rule))

(defun eliza (&optional (rules *eliza-rules*))
  "Respond to user input using pattern matching rules."
  (princ "ELIZA> ")
  (loop :for input := (read-input)
        :if (eq input :quit) :do
          (return-from eliza)
        :else :do
          (format t "~{~a~^ ~}" (flatten (use-eliza-rules input rules)))
          (fresh-line)
          (princ "ELIZA> ")))

(defun use-eliza-rules (input &optional (rules *eliza-rules*))
  "Find some rule with which to transform the input."
  (some #'(lambda (rule)
            (let ((result (match (rule-pattern rule) input)))
              (unless (eq result fail)
                (sublis (switch-viewpoint result)
                        (random-elt (rule-responses rule))))))
        rules))

(defun switch-viewpoint (words)
  "Change I to YOU and vice-versa, and so on."
  (sublis '((i . you) (you . i) (me . you) (am . are)) words))

(defun read-input (&optional (stream *standard-input*))
  "Read input from the user allowing them to include punctuation characters
(e.g. commas, quotes, back-ticks, etc) and return it as a list of symbols that
can be passed to `match'."
  (let ((words) (ss (make-string-output-stream)))
    (labels ((maybe-read-from-string-stream (stream)
               (let ((str (get-output-stream-string stream)))
                 (or (read-from-string str nil nil) str)))
             (maybe-add-to-words (stream)
               (let ((str (maybe-read-from-string-stream stream)))
                 (and (exit-p str) (return-from read-input :quit))
                 (unless (string-empty-p str)
                   (push str words))))
             (exit-p (str) (string-equal *exit-string* str)))
      (loop :for ch :across (read-line stream)
            :if (punctuation-p ch) :do
              (and (maybe-add-to-words ss) (push ch words))
            :else :if (whitespace-p ch) :do
              (maybe-add-to-words ss)
            :else :do (format ss "~c" ch)
            :finally (maybe-add-to-words ss))
      (nreverse words))))
