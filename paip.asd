(defsystem #:paip
  :description "Exercises and examples from the book Paradigms of Artificial Intelligence Programming"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/01
               #:paip/02
               #:paip/03
               #:paip/04
               #:paip/05
               #:paip/06))

(defsystem #:paip/utils
  :description "Utilities for use in the implementations of any of the chapters."
  :version (:read-file-form "version.lisp" :at (0 2))
  :serial t
  :pathname "src/"
  :components ((:file "utils")))

(defsystem #:paip/01
  :description "Exercises and examples from chapter 01"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/utils)
  :serial t
  :pathname "src/01/"
  :components ((:module "examples"
                :components ((:file "examples")))
               (:module "exercises"
                :components ((:file "exercises")))))

(defsystem #:paip/02
  :description "Exercises and examples from chapter 02"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/utils)
  :serial t
  :pathname "src/02/"
  :components ((:module "examples"
                :components ((:file "examples")))
               (:module "exercises"
                :components ((:file "exercises")))))

(defsystem #:paip/03
  :description "Exercises and examples from chapter 03"
  :version (:read-file-form "version.lisp" :at (0 2))
  :serial t
  :pathname "src/03/"
  :components ((:module "examples"
                :components ((:file "examples")))
               (:module "exercises"
                :components ((:file "exercises")))))

(defsystem #:paip/04
  :description "Exercises and the General Problem Solver (GPS) from chapter 04"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/utils)
  :serial t
  :pathname "src/04/"
  :components ((:module "gps"
                :components ((:file "dbg")
                             (:file "gps")))
               (:module "exercises"
                :components ((:file "exercises")))))

(defsystem #:paip/05
  :description "Exercises and ELIZA from chapter 05"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/utils
               #:paip/06)
  :serial t
  :pathname "src/05/"
  :components ((:module "match"
                :components ((:file "match")))
               (:module "eliza"
                :components ((:file "eliza")))
               (:module "exercises"
                :components ((:file "exercises")))))

(defsystem #:paip/06
  :description "Exercises and tools from chapter 06"
  :version (:read-file-form "version.lisp" :at (0 2))
  :depends-on (#:paip/utils)
  :serial t
  :pathname "src/06/"
  :components ((:module "interpreter"
                :components ((:file "interpreter")))
               (:module "match"
                :components ((:file "match")))
               (:module "exercises"
                :components ((:file "exercises")))))
